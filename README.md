# ansible-docker-swarm

Install a two-node docker swarm (1 manager and 1 worker) using this simple playbook.

# Ansible Inventory File

Have the inventory file configured with the manager and worker's IPs and passwords repectively as shown below.

```
[manager]
dockermanager ansible_host=10.10.10.11 ansible_user=cloud_user ansible_ssh_pass=PassW0rd ansible_sudo_pass=PassW0rd

[workers]
dockerworker ansible_host=10.10.10.12 ansible_user=cloud_user ansible_ssh_pass=PassW0rd ansible_sudo_pass=PassW0rd

[swarm:children]
manager
workers
```

# Ansible config file

Use the config file template below, with the inventory pointing to your working directory

```
[defaults]
host_key_checking = no

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o IdentitiesOnly=yes

[defaults]
inventory = inventory
```

# Ansible Playbook

In the `vars` section of the playbook, ensure to plug in your manager's IP

```
vars:
    docker_port: 2377
    docker_manager: 10.10.10.11
    docker_packages:
      - docker-client
      - docker-client-latest
      - docker-common
      - docker-latest
      - docker-latest-logrotate
      - docker-logrotate
      .....
```

# Usage

Override the dafult ansible configuration file (/etc/ansible/ansible.cfg) with the one shipped in this repo using:

`export ANSIBLE_CONFIG=/path/to/new/ansible.cfg`

Run ansible playbook:

`ansible-playbook setup_docker_swarm.yaml`




